﻿#region Class Header
/* ========================================================================
* 【本类功能概述】
* 
* 作者：chinesedragon 
* 时间：4/19/2013 2:21:17 PM
* 文件名：Installer
* 版本：V1.0.1
*
* 修改时间： 
* 修改说明：
* ========================================================================
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Updater
{
    class Installer
    {
        public static void Install(string dirSource, string dirDest)
        {
            if (!Directory.Exists(dirSource))
                return;
            if (!Directory.Exists(dirDest))
                Directory.CreateDirectory(dirDest);

            string[] dirs = Directory.GetDirectories(dirSource);
            foreach (string dir in dirs)
            {
                Install(dir, dirDest + dir.Substring(dir.LastIndexOf("\\")));
            }

            string[] files = Directory.GetFiles(dirSource);
            foreach (string file in files)
            {
                File.Copy(file, dirDest + file.Substring(file.LastIndexOf("\\")), true);
            }
        }
    }
}
