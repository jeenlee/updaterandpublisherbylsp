﻿#region Class Header
/* ========================================================================
* 【本类功能概述】
* 
* 作者：chinesedragon 
* 时间：4/17/2013 8:48:16 PM
* 文件名：ReleaseFile
* 版本：V1.0.1
*
* 修改时间： 
* 修改说明：
* ========================================================================
*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


/// <summary>
/// 发布的文件
/// </summary>
public class ReleaseFile
{
    public string FileName { get; set; }            //文件名
    public long FileSize { get; set; }              //文件大小
    public string Version { get; set; }         //发布日期
}
