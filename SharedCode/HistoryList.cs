﻿using System.Collections.Generic;
using System;

public class HistoryList
{
    private List<HistoryItem> items;

    public List<HistoryItem> HistoryItems
    {
        get
        {
            return items ?? (items = new List<HistoryItem>());
        }
        set
        {
            items = value;
        }
    }

    public void InsertItem(string name, string path, string type)
    {
        HistoryItem item = new HistoryItem
        {
            ProjectName = name,
            ProjectFullPath = path,
            ProjectType = type,
        };
        InsertItem(item);
    }

    public void InsertItem(HistoryItem item)
    {
        if (item == null)
        {
            return;
        }

        if (items == null)
        {
            items = new List<HistoryItem>();
        }
        foreach (HistoryItem i in items)
        {
            if (i.ProjectName.CompareTo(item.ProjectName) == 0
                && i.ProjectFullPath.CompareTo(item.ProjectFullPath) == 0
                && i.ProjectType.CompareTo(item.ProjectType) == 0)
            {
                items.Remove(i);
                break;
            }
        }
        items.Insert(0, item);
    }
}
