﻿using System;

public class HistoryItem
{
    public string ProjectName { get; set; }
    public string ProjectFullPath { get; set; }
    public string ProjectType { get; set; }
}
