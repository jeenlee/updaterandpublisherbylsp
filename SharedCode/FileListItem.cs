﻿using System;

public class FileListItem
{
    public string Dir { get; set; }
    public bool IsSelected { get; set; }
    public bool IsFile { get; set; }
}
